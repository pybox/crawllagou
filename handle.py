#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import logging
import Queue
import threading
from collections import Counter
from elasticsearch import Elasticsearch
from cfg import *

Logging = logging.getLogger("lagou-merge.log")
handler = logging.FileHandler(Logging.name)
handler.setFormatter(logging.Formatter('%(asctime)s %(levelname)-8s %(message)s'))
Logging.addHandler(handler)
Logging.setLevel(logging.DEBUG)
Logging.addHandler(handler)

es = Elasticsearch(host="192.168.25.75", port=9200)
Thread_SIZE = 5
Queue_SIZE = Queue.Queue()


def handle():
    files_arr = []
    for root, dirs, files in os.walk(COLLECT_DIR):
        for name in files:
            if name.rsplit(".", 1)[-1] == "json":
                files_arr.append(os.path.join(root, name))

    return iter(files_arr)


def save_in_es(multi=False):
    if not es.indices.exists(index="lagou"):
        print es.indices.create(index="lagou")

    if multi is True:
        # Exist BUG: ConflictError! document_already_exists_exception 409
        while True:
            if not Queue_SIZE.empty():
                item = Queue_SIZE.get()

                with open(item, "rb") as rf:
                    content = rf.read().encode("utf8")
                    content = eval(content)

                for text in content["content"]["result"]:
                    if es.exists(index="lagou", doc_type="position", id=text["positionId"]):
                        Logging.warning("%s has exists... appear in %s" % (text["positionId"], repr(item)))
                    else:
                        es.create(index="lagou", doc_type="position", id=text["positionId"], body=text)

                Queue_SIZE.task_done()

    else:
        # 得到所有文件
        files_arr = handle()

        for item in files_arr:
            with open(item, "rb") as rf:
                content = rf.read().encode("utf8")
                content = eval(content)

            for text in content["content"]["result"]:
                if es.exists(index="lagou", doc_type="position", id=text["positionId"]):
                    Logging.warning("%s has exists... appear in %s" % (text["positionId"], repr(item)))
                    # print es.index(index="lagou", doc_type="position", id=text["positionId"], body=text)
                    # Logging.debug("new_version: /lagou/position/%s" % text["positionId"])
                else:
                    es.create(index="lagou", doc_type="position", id=text["positionId"], body=text)


def multi_save_in_es():
    for i in xrange(Thread_SIZE):
        t = threading.Thread(target=save_in_es, name=u"thread-%02d" % (i + 1), kwargs={"multi": True})
        t.setDaemon(True)
        t.start()

    files_arr = handle()
    for content_item in files_arr:
        Queue_SIZE.put(content_item)
    Queue_SIZE.join()


def save_in_csv(datalt=None, save_dir=None, file_name="save_in_csv.csv"):
    import pandas as pd
    from json import loads as jsl
    import gc

    save_dir = save_dir or CSV_DIR
    _result_, _res_ = {}, []

    # 得到所有文件
    files_arr = datalt or handle()

    for item in files_arr:
        with open(item, "rb") as _rf:
            try:
                _content = _rf.read().encode("utf8")
            except UnicodeDecodeError:
                _content = _rf.read().encode("gbk")

            _content = jsl(_content)

            try:
                _res_.extend(_content["content"]["result"])
            except KeyError:  # lagou的 API更新了
                _res_.extend(_content["content"]["positionResult"]["result"])
            except Exception, e:
                raise Exception(str(e) + " in " + repr(item))

        del _rf, _content
        gc.collect()

    _result_ = {item["positionId"]: item for item in _res_}
    # TO CSV. Use utf8 with BOM cause excel
    _v = _result_.values()
    _v.sort(key=lambda k: k["createTimeSort"], reverse=True)
    a = pd.DataFrame.from_dict(_v, orient='columns', dtype=None)
    a.to_csv(os.path.join(save_dir, file_name), encoding="utf-8-sig")
    print "finished. The file save in : %s" % os.path.join(CSV_DIR, file_name)


def search_in_es(query=None, from_=0):
    """
    # https://www.elastic.co/guide/en/elasticsearch/reference/current/search-uri-request.html
    # https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-query-string-query.html
    """
    # {"query": {"bool": {"must": [{ "match_phrase": { "city": "佛山" } }, { "match": { "positionName": "师" } } ] } }, "_source": ["positionName", "city", "education", "salary"], "size":10, "from":20 }
    res = es.search(index="lagou",
                    size=50,
                    from_=from_,
                    request_cache=True,
                    body='''{
                        "_source": ["positionFirstType", "positionType", "positionName",
                                    "city", "education", "workYear", "salary",
                                    "industryField", "financeStage", "companyLabelList"]
                        }''',
                    )

    total = res["hits"]["total"]
    return total, from_, [item["_source"]["salary"] for item in res["hits"]["hits"]]


def trans_type():
    """改变存储类型为json.dumps"""
    import json
    # 得到所有文件
    files_arr = handle()

    for __item in files_arr:
        with open(__item, "rb") as _rf:
            try:
                __content = _rf.read().encode("utf8")
            except UnicodeDecodeError:
                __content = _rf.read().encode("gbk")

            try:
                __content = json.loads(__content)
            except ValueError:
                print __item
                __content = eval(__content)
                if not isinstance(__content, dict):
                    __content = json.loads(__content)

                with open(__item, "wb") as _wf:
                    _wf.write(json.dumps(__content))


def save_in_sqlite():
    import sqlite3
    from json import loads as jsl

    _count, _cf_size = 0, 500
    conn = sqlite3.connect(DB_DIR)

    def add_sql(dc, __count):
        _k = ", ".join(dc.keys())
        _v = ":" + ", :".join(dc.keys())
        c = conn.cursor()
        query = "INSERT INTO datas (%s) VALUES (%s)" % (_k, _v)
        c.execute(query, dc)

        # Save (commit) the changes
        if __count == _cf_size:
            conn.commit()

    files_arr = handle()
    for item in files_arr:
        # print "now is in:", item
        # read data
        with open(item, "rb") as _rf:
            try:
                _content = _rf.read().encode("utf8")
            except UnicodeDecodeError:
                _content = _rf.read().encode("gbk")

            _content = jsl(_content)

            if _content.get("content", {}).get("result"):
                _data = _content["content"]["result"]
            elif _content.get("content", {}).get("positionResult", {}).get("result"):
                _data = _content["content"]["positionResult"]["result"]  # lagou的 API更新了
            else:
                print Exception("_data Not Found" + " in " + repr(item))

            if _data:
                for _item in _data:
                    _dc = {}
                    for _k, _v in _item.items():
                        if isinstance(_v, list):
                            _dc[_k] = ", ".join(_v)
                        else:
                            _dc[_k] = _v
                    _count += 1
                    add_sql(_dc, _count)
                    if _count == _cf_size:
                        _count = 0

    conn.close()


def db_clear():
    import sqlite3
    conn = sqlite3.connect(DB_DIR)
    c = conn.cursor()
    query = "delete from datas where datas.rowid not in (select MAX(datas.rowid) from datas group by positionId);"
    c.execute(query)
    query = "vacuum;"
    c.execute(query)
    conn.close()
