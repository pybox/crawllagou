#!/usr/bin/env python
# -*- coding: utf-8 -*-
import re
import time
import urllib
import requests
import functools

DEBUG = True
req_exc = requests.exceptions


def trace_dbg(func):
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        if DEBUG:
            print func.__name__
        return func(*args, **kwargs)

    return wrapper


class BASE_PROXY(object):
    IP_RULE = re.compile('(?<=href="/)[0-9./]*(?=">)')
    Header = {"User-Agent": "Mozilla/5.0 (Windows NT 6.3; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0", }

    @staticmethod
    @trace_dbg
    def proxy(page=5):
        u"""获取代理, 处理分页"""
        _result = []
        for i in xrange(page):
            proxy_lt = BASE_PROXY.get_proxy(offset=25 * i) or False
            if proxy_lt:
                time.sleep(1)
                _result.extend(proxy_lt)

        return _result

    @staticmethod
    @trace_dbg
    def get_proxy(offset=0, _retry=0):
        u"""获取代理"""
        if _retry > 3:
            raise Exception("Network Error!!!")

        try:
            _data = {'protocol': 'http', 'limit': '25', 'offset': offset, 'minavail': '0', 'maxtime': '0',
                     'anonlvl': '2'}
            url_part = urllib.urlencode(_data) + "&anonlvl=3&anonlvl=4"

            resp = requests.get("http://proxydb.net/?" + url_part, headers=BASE_PROXY.Header)
            if resp.status_code == 200:
                res = re.findall(BASE_PROXY.IP_RULE, resp.text)
                return [item.replace("/", ":") for item in res if item]
            else:
                raise req_exc.ConnectionError

        except req_exc.ConnectionError:
            _retry += 1
            BASE_PROXY.get_proxy(offset=offset, _retry=_retry)

    @staticmethod
    @trace_dbg
    def test_proxy(ip_port, timeout=3):
        u"""测试代理是否连通"""
        proxy = {"http": ip_port}
        try:
            resp = requests.get("http://www.lagou.com", headers=BASE_PROXY.Header, proxies=proxy, timeout=timeout)
            if resp.status_code == 200:
                return True

        except (req_exc.Timeout, req_exc.ProxyError, req_exc.ConnectionError, req_exc.TooManyRedirects):
            return False

        return False

    @staticmethod
    @trace_dbg
    def get_vail_proxy(page=3, limit=20, _wf=True, **kwargs):
        u"""测试后，返回有效结果
        page, 获取前x页的代理信息
        limit, 指定返回的数量，0 为通过测试的所有代理
        base_path, 代理信息存放的位置
        """
        _real = []
        result = BASE_PROXY.proxy(page)

        for item in result:
            if BASE_PROXY.test_proxy(ip_port=item):
                _real.append(item)

            if limit != 0 and len(_real) == limit:
                break

        if _wf:
            if kwargs.has_key("base_path"):
                BASE_PROXY.save_proxy(_real, base_path=kwargs["base_path"])
            BASE_PROXY.save_proxy(_real)

        return _real

    @staticmethod
    @trace_dbg
    def save_proxy(result, base_path=""):
        from os import path
        from json import dumps as jsd

        w_content = {"time": time.time(), "result": result}
        with open(path.join(base_path, "proxy.dat"), "wb") as wf:
            wf.write(jsd(w_content))
