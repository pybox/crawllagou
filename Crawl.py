#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import time
import Queue
import threading
import functools
from json import dumps as jsd
from json import loads as jsl

import requests

from cfg import *
import proxy as pxy
from handle import save_in_sqlite, db_clear

"""
# http://www.lagou.com/jobs/positionAjax.json?px=new
# http://elasticsearch-py.readthedocs.io
# https://raw.githubusercontent.com/wiki/Datartisan/knowledge/elasticsearch-py-notes.md
# http://proxydb.net/
"""

DEBUG = True
REST = 5 * 60 * 60  # 每5小时执行一次

ACTION_TIME = ""
geTime = lambda: time.strftime("%Y-%m-%d_%H_%M_%S", time.localtime(time.time()))
TIME_NAME = geTime()


def trace_dbg(func):
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        if DEBUG:
            print func.__name__
        return func(*args, **kwargs)

    return wrapper


def catchKeyboardInterrupt(func):
    def wrapper(*args):
        try:
            return func(*args)
        except KeyboardInterrupt:
            print u"\n[*] 强制退出程序"
            exit()

    return wrapper


class Crawl(object):
    Header = {
        "Accept-Language": "zh-CN,zh;q=0.8",
        "Accept-Encoding": "gzip, deflate",
        "User-Agent": "Mozilla/5.0 (Windows NT 6.3; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0"
    }
    browser = requests.session()

    def __init__(self, uri, thread_size=3, queue_size=5, visited=(False, None),
                 proxy=False, faker_ip=False, ip=None, **kwargs):
        """
          uri, 需要访问的地址
          visited, 是否需要初次访问
          proxy, 是否需要代理, True 会使用set_proxy方法获取代理信息，并应用
            **kwargs: PROXY_SERVER={"proxy_mode": "proxy_addr"} 可自定义代理地址
          faker_ip, 设置HTTP Header的 ip
          ip, 若 faker_ip为 True, 则设置 faker_ip的 ip值
        """
        self.URI = uri
        self.PROXY = proxy
        self.Thread_SIZE = thread_size
        self.Queue_SIZE = Queue.Queue(queue_size)

        if kwargs:
            for k, v in kwargs.items():
                setattr(self, k, v)

        if faker_ip:
            ip = ip or "8.8.8.8"
            _headers = {
                "X-Forwarded-For": ip,
                "HTTP_CLIENT_IP": ip,
                "REMOTE_ADDR": ip,
                "User-Agent": "Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)",
            }
            self.Header.update(_headers)

        if proxy:
            self.set_proxy()

        if visited[0]:
            self._first_visit(url=visited[1])

    def _first_visit(self, url=None, retry=0, **kwargs):
        if retry > 3:
            raise Exception("Network Error")

        uri = url or self.URI

        _herder = {k: v for k, v in self.Header.items()}
        _herder.update({
            "Host": "www.lagou.com",
            "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
            "Upgrade-Insecure-Requests": "1"
        })

        if hasattr(self, "PROXY_SERVER"):
            proxy_server = self.PROXY_SERVER or False
            if proxy_server:
                kwargs = {"proxies": proxy_server}
                # test proxy
                # _test = self.browser.get("http://pv.sohu.com/cityjson", headers=_herder, timeout=30, **kwargs).text

        try:
            _ = self.browser.get(uri, headers=_herder, timeout=30, **kwargs)

            if _.status_code < 400:
                return
            else:
                retry += 1
                self._first_visit(url=url, retry=retry, **kwargs)
        except requests.exceptions.ReadTimeout:
            print "ReadTimeout!"
            if hasattr(self, "PROXY_SERVER"):
                self.set_proxy(refresh=True)
            self._first_visit(url=url, retry=retry, **kwargs)

    @trace_dbg
    def spider_work(self, page=1, **kwargs):
        _herder = {k: v for k, v in self.Header.items()}
        _calb_hearder = {k: v for k, v in self.Header.items()}
        _herder.update({
            "Origin": "http://www.lagou.com",
            "Accept": "application/json, text/javascript, */*; q=0.01",
            "Host": "www.lagou.com",
            "Referer": "http://www.lagou.com/zhaopin/",
            "X-Requested-With": "XMLHttpRequest",
            "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8"
        })
        _calb_hearder.update({"Host": "c.lagou.com", "Referer": "http://www.lagou.com/zhaopin/"})

        payload = {'kd': '', 'pn': page, 'first': False, }

        if hasattr(self, "PROXY_SERVER"):
            proxy_server = self.PROXY_SERVER or False
            if proxy_server:
                kwargs = {"proxies": proxy_server}

        resp = self.browser.post(self.URI, data=payload, headers=_herder, timeout=30, **kwargs)
        if resp.status_code == 200:
            _params = {
                "callback": "",
                "type": "COMPANY",
                "ids": ",".join(self.copID(resp.json())),
                "_": str(int(time.time() * 1000))
            }

            # callback
            _ = self.browser.get("http://c.lagou.com/icon/showIcon.json", params=_params, headers=_calb_hearder)

            return {"cur_page": page, "text": jsd(resp.json())}
            # return {"cur_page": page, "text": resp.json()}
        else:
            raise requests.ConnectionError

    @trace_dbg
    def set_proxy(self, refresh=False):
        if os.path.isfile(os.path.join(base_dir, "proxy.dat")):
            with open(os.path.join(base_dir, "proxy.dat"), "rb") as rf:
                _proxy_info = jsl(rf.read())
            _proxy_arr = _proxy_info.get("result", "")

            if time.time() - _proxy_info.get("time", 0) > 3 * 24 * 60 * 60:
                pxy.BASE_PROXY.get_vail_proxy(page=2, limit=15, _wf=True, base_path=base_dir)

            if refresh and hasattr(self, "PROXY_NUM") and hasattr(self, "PROXY_ARR"):
                # remove first
                _proxy_arr.remove(self.PROXY_ARR[self.PROXY_NUM])
                pxy.BASE_PROXY.save_proxy(_proxy_arr, base_path=base_dir)

                # refresh end
                self.PROXY_NUM += 1
                if self.PROXY_NUM < len(self.PROXY_ARR):
                    setattr(self, "PROXY_SERVER", {"http": self.PROXY_ARR[self.PROXY_NUM]})
                    return
                else:
                    pxy.BASE_PROXY.get_vail_proxy(page=2, limit=15, _wf=True, base_path=base_dir)
                    return self.set_proxy()

            setattr(self, "PROXY_ARR", _proxy_info.get("result", ""))
            setattr(self, "PROXY_NUM", 0)
            setattr(self, "PROXY_SERVER", {"http": self.PROXY_ARR[self.PROXY_NUM]})
            return

        else:
            pxy.BASE_PROXY.get_vail_proxy(page=2, limit=15, _wf=True, base_path=base_dir)

        return self.set_proxy()

    @staticmethod
    def copID(__json):
        """use for callback"""
        __result = []
        try:
            for item in __json["content"]["positionResult"]["result"]:
                __result.append(str(item["companyId"]))

            return __result
        except:
            return []

    # @classmethod
    def workLogic(self, page_num, retry=0, rest_time=3):
        if retry > 5:
            print u"page_num: %03d, kill straight cause retry too more..." % page_num
            return

        try:
            kw = self.spider_work(page_num)
            _path = os.path.join(COLLECT_DIR, TIME_NAME)
            if not os.path.exists(_path):
                os.makedirs(_path)

            with open(os.path.join(_path, "%03d.json" % page_num), "wb") as wf:
                wf.write(kw["text"])

            return

        except (requests.exceptions.ReadTimeout, requests.ConnectionError, WindowsError, ValueError):
            _ = geTime()
            print u"In %s: Network connects error！ at page: %s" % (_, page_num)
            time.sleep(rest_time)

            retry += 1
            self.workLogic(page_num, retry=retry)
        except requests.TooManyRedirects:
            if hasattr(self, "PROXY_SERVER"):
                self.set_proxy(refresh=True)
            self.workLogic(page_num, retry=0)

    # @staticmethod
    def worker(self):
        while True:
            if not sq.empty():
                item = sq.get()

                # DO SOMETHINGS
                self.workLogic(item)

                sq.task_done()
                time.sleep(1.5)


@trace_dbg
@catchKeyboardInterrupt
def main(url="http://www.lagou.com/jobs/positionAjax.json?px=new"):
    global spider, TIME_NAME, sq
    spider = Crawl(url, visited=(True, "http://www.lagou.com/zhaopin/"), proxy=True, faker_ip=False)
    sq = spider.Queue_SIZE
    for i in xrange(spider.Thread_SIZE):
        t = threading.Thread(target=spider.worker, name=u"thread-%02d" % (i + 1))
        t.setDaemon(True)
        t.start()

    while True:
        import random
        lt = range(1, 335)
        random.shuffle(lt)

        for number in lt:
            sq.put(number)
        sq.join()

        # 休息间隔
        print u"The times mission '%s' is finished，wait for %ss to start the next ...\n" % (TIME_NAME, REST)
        save_in_sqlite()
        import shutil
        shutil.rmtree(os.path.join(COLLECT_DIR, TIME_NAME))
        db_clear()
        time.sleep(REST-300)
        # for _ in xrange(REST):
        #     print "\rTime left:! %s" % str(REST - _),
        #     sys.stdout.flush()
        #     time.sleep(1)

        TIME_NAME = geTime()
        print u"Ready for working the times mission in: %s ..." % TIME_NAME
        spider = Crawl(url, visited=(True, "http://www.lagou.com/zhaopin/"), proxy=True, faker_ip=False)

# main(url="http://www.lagou.com/jobs/positionAjax.json?px=new&city=%E5%B9%BF%E5%B7%9E&needAddtionalResult=false")
# main()
