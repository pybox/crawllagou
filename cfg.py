from os import path

base_dir = path.abspath(path.dirname(__file__))
DATA_DIR = path.join(base_dir, "data")

# COLLECT_DIR = path.join(base_dir, "data_gz")    # GZ
# CSV_DIR = path.join(COLLECT_DIR, "csv")    # GZ
COLLECT_DIR = path.join(DATA_DIR, "crawl")
GongSi_DIR = path.join(DATA_DIR, "gongsi")
CSV_DIR = path.join(DATA_DIR, "csv")
CHART_DIR = path.join(DATA_DIR, "chart")
Cache_DIR = path.join(DATA_DIR, "cache")
# TEST_DIR = path.join(base_dir, "tests")

DB_DIR = path.join(DATA_DIR, "crawl", "lagou.db")
