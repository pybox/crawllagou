#!/usr/bin/env python
# -*- coding: utf-8 -*-

import re
import pandas as pd
import cufflinks as cf
from cfg import *
import plotly
from plotly.graph_objs import Scatter, Layout, Figure

cf.go_offline()


class BASE_CSV(object):
    DATA = pd.read_csv(path.join(CSV_DIR, "save_in_csv.csv"), low_memory=False, index_col=0)

    @staticmethod
    def draw_chart(data, keyword, kind="bar", **kw):
        _kw = {
            "sort": True,
            "textinfo": 'label+percent',
            "theme": "ggplot",
            "textposition": "outside",
            "pull": 0.015,
        }
        _kw.update(kw)

        # __data = self.DATA[keyword]
        # __sum_counts = __data.value_counts()[:top]
        #
        # if _df and chart:
        #     __sum_counts_df = __sum_counts.to_frame()
        #     return __sum_counts_df

        dt = {idx: {keyword: v[0], "number": v[1]} for idx, v in enumerate(data.to_dict()[keyword].items())}
        df = pd.DataFrame(dt).T

        if kind in ["scatter", "bar", "box", "heatmap"]:
            df = data
            if not isinstance(data, pd.core.frame.DataFrame):
                df = data.to_frame()
        return df.iplot(kind=kind, labels=keyword, values="number", online=False, **_kw)

    def sas_city(self, get_data="", chart=False, top=0, **kw):
        """得到岗位中，岗位所在城市的数量"""
        __type = "city"
        __data = self.DATA[__type]  # pandas.core.series.Series

        if top:
            __data = __data.value_counts()[:top]  # pandas.core.series.Series

        if get_data == "df":
            __data_df = __data.to_frame()
            return __data_df
        elif get_data == "se":
            return __data

        if chart:
            __data_df = __data.to_frame()
            return self.draw_chart(data=__data_df, keyword=__type, **kw)

    def sas_edu(self, get_data="", chart=False, top=0, **kw):
        """得到岗位描述中，职位学历要求的分布"""
        __type = "education"
        __data = self.DATA[__type]  # pandas.core.series.Series

        if top:
            __data = __data.value_counts()[:top]  # pandas.core.series.Series

        if get_data == "df":
            __data_df = __data.to_frame()
            return __data_df
        elif get_data == "se":
            return __data

        if chart:
            __data_df = __data.to_frame()
            return self.draw_chart(data=__data_df, keyword=__type, **kw)

    def sas_salary(self, data=None, get_data="", chart=False, top=0, **kw):
        """得到岗位描述中，岗位薪资的分布"""
        rule = re.compile(r"[\x80-\xff]{3}")

        __type = "salary"
        if data is not None:
            __data = data[__type]
        else:
            __data = self.DATA[__type]  # pandas.core.series.Series

        # patch: Chinese Char
        __lt = __data.tolist()
        __res = []
        for text in __lt:
            text = re.sub(rule, "", text)
            text = text.replace("k", "")
            text = text.split("-")
            if len(text) == 2:
                __res.append("%sK - %sK" % (text[0], text[1]))
            else:
                __res.append("%sK - %sK" % (text[0], text[0]))
        __data = pd.core.series.Series(__res)

        if top:
            __data = __data.value_counts()[:top]  # pandas.core.series.Series

        if get_data == "df":
            __data_df = __data.to_frame("salary")
            return __data_df
        elif get_data == "se":
            return __data

        if chart:
            __data_df = __data.to_frame("salary")
            return self.draw_chart(data=__data_df, keyword=__type, **kw)

    def sas_workyear(self, get_data="", chart=False, top=0, **kw):
        """得到岗位描述中，岗位薪资的分布"""
        __map = {"1-3": "1-3年", "3-5": "3-5年", "5-10": "5-10年", "不限年": "不限", "1年以下年": "1年以下", "无经验年": "应届毕业生"}

        __type = "workYear"
        __data = self.DATA[__type]  # pandas.core.series.Series
        for item in __data.index:
            if __map.get(__data[item]):
                __data[item] = __map.get(__data[item])

        if top:
            __data = __data.value_counts()[:top]  # pandas.core.series.Series

        if get_data == "df":
            __data_df = __data.to_frame()
            return __data_df
        elif get_data == "se":
            return __data

        if chart:
            __data_df = __data.to_frame()
            return self.draw_chart(data=__data_df, keyword=__type, **kw)

    def sas_positionType(self):
        """得到岗位描述中，具体岗位类型的分布"""
        __type = "positionType"

    def sas_industry(self):
        """得到岗位描述中，行业的分布"""
        __type = "industryField"

    def sas_cop_size(self):
        """得到岗位描述中，公司规模的分布"""
        __type = "companySize"

    def sas_cop_financeStage(self):
        """得到岗位描述中，公司发展阶段的分布"""
        __type = "financeStage"

    def sas_cop_label(self, _df=True, chart=False, top=20):
        """得到岗位描述中，各标签的数量"""
        __type = "companyLabelList"


csv = BASE_CSV()
csv.sas_city(_df=False, chart=True, filename="test")


def chart_of_salary():
    from collections import Counter

    rule = re.compile(r"[\x80-\xff]{3}")
    a = BASE_CSV.DATA
    lt = a["salary"].tolist()
    res = []
    for text in lt:
        text = re.sub(rule, "", text)
        text = text.replace("k", "")
        text = text.split("-")
        if len(text) == 2:
            res.append(",".join(text))
        else:
            res.append(",".join(text + text))

    xlt, ylt, size_lt = [], [], []
    # for k, v in Counter(res).most_common(20):
    for k, v in Counter(res).items():
        s = k.split(",")
        xlt.append(s[0])
        ylt.append(s[1])
        size_lt.append(v)
    size_lt = [round(i * 1000 / sum(size_lt), 2) for i in size_lt]  # 按百分比缩放

    trace0 = Scatter(
        x=xlt,
        y=ylt,
        mode='markers',
        marker=dict(
            size=size_lt,
        )
    )
    data = [trace0]
    layout = Layout(
        title=u"拉勾网职位薪资气泡图",
        xaxis=dict(
            title=u"薪资下限",
        ),
        yaxis=dict(
            title=u"薪资上限",
        ),
        paper_bgcolor='rgb(243, 243, 243)',
        plot_bgcolor='rgb(243, 243, 243)',
    )
    fig = Figure(data=data, layout=layout)
    plot_url = plotly.offline.plot(fig, filename=u"拉勾网职位薪资气泡图")

    return plot_url


def chart_of_city(xlt, ylt, size_lt):
    trace0 = Scatter(
        x=xlt,
        y=ylt,
        mode='markers',
        marker=dict(
            size=size_lt,
        )
    )
    data = [trace0]
    layout = Layout(
        title=u"拉勾网职位地区分布气泡图",
        xaxis=dict(
            title=u"地区",
        ),
        yaxis=dict(
            title=u"数量",
        ),
        paper_bgcolor='rgb(243, 243, 243)',
        plot_bgcolor='rgb(243, 243, 243)',
    )
    fig = Figure(data=data, layout=layout)
    plot_url = plotly.offline.plot(fig, filename=u"拉勾网职位薪资气泡图")

    return plot_url
